org 0x100
use16

; ARG
; Просмотр количества байтов в аргументах.
; xor bx, bx
; mov bl, [0x80]
; cmp bl, 7Eh
; ja exit ; аргументов больше, чем 126 <=> не менее, чем 127

; mov byte [bx + 0x81], 0x24
; mov ah, 9
; mov dx, 0x82
; int 0x21

xor di, di
mov al, [0x82]
cmp al, 0x2d
jne make_argument_int_part_1
mov word [sign_arg], -1
inc di

make_argument_int_part_1:
xor ax, ax
mov cx, 10

fill_digit_part_1:
mov byte bl, [0x82 + di]
cmp bl, 0x2e
je make_argument_int_part_0
inc di

mul cx
sub bl, 48
add ax, bx
jmp fill_digit_part_1

make_argument_int_part_0:
mov [argument_int_part_1], ax
xor ax, ax
inc di

fill_digit_part_0:
mov byte bl, [0x82 + di]
cmp bl, 0xd
je make_argument_float
inc di

mul cx
sub bl, 48
add ax, bx
jmp fill_digit_part_0

make_argument_float:
mov [argument_int_part_0], ax
fild word [argument_int_part_0]
fld qword [v_10pm2]
fmulp
fild word [argument_int_part_1]
faddp
fild word [sign_arg]
fmulp

; SIN
; fld qword [x]
fld qword [v_180]
fdivp
; st(0) = x / 180

fldpi
fmulp
; st(0) = x / 180 * pi

fstp qword [x]

fld qword [x]
fmul qword [x]
fstp qword [xx]

mov cx, [n]
iter:

mov ax, cx
dec cx
mul cx
mov word [f], ax

fld1
fld qword [answer]
fld qword [xx]
fild word [f]

fdivp
fmulp
fsubp
fstp qword [answer]

dec cx
xor ax, ax
cmp cx, 1
jg iter

fld qword [answer]
fld qword [x]
fmulp
fst qword [answer]
; st(0) = sin(x)

; Узнавание знака ответа.
fld qword [answer]
fldz
fcompp
fstsw [word_status]

; mov ax, 0x100 ; 2^8
; mov word bx, [word_status]
; and ax, bx
; cmp ax, 0
; jne make_partition

; C3.
mov ax, 0x4000; 2^14
mov word bx, [word_status]
and ax, bx
cmp ax, 0x4000
je make_partition

; C2.
mov ax, 0x100 ; 2^8
mov word bx, [word_status]
and ax, bx
cmp ax, 0
jne make_partition
mov byte [sign], 0

make_partition:
fabs
fild dword [v_10p5]
fmulp
; st(0) = sin(x) * (10**5)

fild dword [v_10]
fmulp
fistp dword [answer_int_part_1]

mov dword eax, [answer_int_part_1]
mov edx, 0
mov dword ecx, [v_10]
div ecx
mov dword [answer_int_part_1], eax

fld qword [answer]
fabs
fild dword [v_10p5]
fmulp
fild dword [answer_int_part_1]
fsubp

fild dword [v_10p5]
fmulp
fistp dword [answer_int_part_0]

; Применяется при отладке.
; fild dword [answer_int_part_1]
; fild dword [answer_int_part_0]
; fild qword [answer]

mov ecx, 10

mov dword eax, [answer_int_part_1]
cmp eax, 100000
jl fill_part_1
mov byte [answer_str + 1], 49
sub eax, 100000

; Заполнение 1-й части.
fill_part_1:
mov di, answer_str + 7

write_digit_part_1:
xor edx, edx
div ecx
add dl, 48
mov [di], dl
dec di
cmp eax, 0
jne write_digit_part_1

; Заполнение 0-й части.
fill_part_0:
mov dword eax, [answer_int_part_0]
mov di, answer_str + 12

write_digit_part_2:
xor edx, edx
div ecx
add dl, 48
mov [di], dl
dec di
cmp eax, 0
jne write_digit_part_2

; Вывод результ-строки.
mov ah, 0x9
mov dx, answer_str
add byte dl, [sign]
int 0x21

; Завершение работы.
mov ah, 0x4c
mov al, 0
int 0x21

; Аргумент, от которого считается значение функции.
x:
dq 0.0

; Наибольший показатель степени x в формуле Тейлора.
; В разложении sin(x) участвуют только нечётные степени.
n:
dw 13

xx:
dq 0.0

f:
dw 0

v_180:
dq 180.0

v_10p5:
dd 100000

v_10:
dd 10

answer:
dq 1.0

answer_int_part_0:
dd 0

answer_int_part_1:
dd 0

answer_str:
db "-0.0000000000", 0x24

sign:
db 1

word_status:
dw 0

; ARG
sign_arg:
dw 1

argument_int_part_1:
dw 0

argument_int_part_0:
dw 0

argument_float:
dq 0

v_10pm2:
dq 0.01
